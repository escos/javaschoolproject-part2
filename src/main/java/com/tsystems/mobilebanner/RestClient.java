package com.tsystems.mobilebanner;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.api.client.config.ClientConfig;
import com.tsystems.mobilebanner.model.TariffSimpleDto;
import org.apache.log4j.*;

import javax.ejb.Stateless;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Stateless
public class RestClient {

    private final static Logger logger = org.apache.log4j.Logger.getLogger(RestClient.class);

    public List<TariffSimpleDto> getTariffs() {

        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);

        Client client = Client.create(clientConfig);
        WebResource webResource = client.resource("http://localhost:8080/mymobile/main/updated_tariffs");
        ClientResponse response = webResource
                .accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_JSON)
                .get(ClientResponse.class);
        logger.info("Response received!");
        return response.getEntity(new GenericType<List<TariffSimpleDto>>(){});
    }
}

