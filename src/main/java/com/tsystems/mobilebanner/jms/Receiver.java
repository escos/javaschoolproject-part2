package com.tsystems.mobilebanner.jms;

import com.tsystems.mobilebanner.RestClient;
import com.tsystems.mobilebanner.controller.TariffController;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;

import javax.ejb.Startup;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueSession;
import javax.jms.QueueReceiver;

import javax.jms.QueueConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.util.Hashtable;

@Startup
@Singleton
public class Receiver {

    private final static Logger logger = Logger.getLogger(Receiver.class);

    @EJB
    private RestClient restClient;
    @EJB
    private TariffController tariffController;

    private QueueReceiver receiver;
    private QueueConnection connection;
    private QueueSession session;

    @PostConstruct
    public void receive(){
        try{Hashtable<String, String> props = new Hashtable<String, String>();
            props.put("java.naming.factory.initial", "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
            props.put("java.naming.provider.url", "tcp://localhost:61616");
            props.put("queue.js-queue", "myQueue");
            props.put("connectionFactoryNames", "queueCF");

            Context context = new InitialContext(props);
            QueueConnectionFactory connectionFactory;
            connectionFactory = (QueueConnectionFactory) context.lookup("queueCF");
            Queue queue = (Queue) context.lookup("js-queue");
            QueueConnection connection = connectionFactory.createQueueConnection();
            connection.start();
            QueueSession session = connection.createQueueSession(false, QueueSession.AUTO_ACKNOWLEDGE);
            QueueReceiver receiver = session.createReceiver(queue);
            receiver.setMessageListener(new MyMessageListener(restClient,tariffController));
        }catch (NamingException e){
            logger.error("Error in context or queue",e);
        } catch (JMSException e){
            logger.error("JMS error",e);
        }

    }
    @PreDestroy
    public void destruct(){
        if (session != null){
            try {
                session.close();
                connection.close();
                receiver.close();
            } catch (JMSException e) {
                logger.error("Error when resources was closing", e);
            }
        }


    }
}
