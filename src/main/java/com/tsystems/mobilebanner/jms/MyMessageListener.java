package com.tsystems.mobilebanner.jms;

import com.tsystems.mobilebanner.RestClient;
import com.tsystems.mobilebanner.controller.TariffController;
import com.tsystems.mobilebanner.model.TariffSimpleDto;
import org.apache.log4j.Logger;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.List;

public class MyMessageListener implements MessageListener {

    final static Logger logger = Logger.getLogger(MyMessageListener.class);

    private RestClient restClient;

    private TariffController tariffController;

    public MyMessageListener(RestClient restClient, TariffController tariffController) {
        this.restClient = restClient;
        this.tariffController = tariffController;
    }

    public void onMessage(Message message) {
        TextMessage m = (TextMessage) message;
        try {
            System.out.println("message received: "  +
                    "   tariff number:  " +
                    m.getLongProperty("long") + " string property:  " + m.getStringProperty("string"));
            logger.info("Message received!  message: "+ m.getLongProperty("long") + m.getStringProperty("string"));
            List<TariffSimpleDto> tariffSimpleDtos = restClient.getTariffs();
            logger.info("Data received from server");
            tariffController.setTariffs(tariffSimpleDtos, m.getLongProperty("long"));
        } catch (JMSException e) {
            logger.error("Sorry, JMS error!", e);
        }

    }
}
