package com.tsystems.mobilebanner.homework;


import javax.ejb.Stateless;
import java.io.Serializable;


@Stateless
public class UserEJB implements Serializable {

    public String sayHello(String name) {

        return "Hello " + name;
    }



}

