package com.tsystems.mobilebanner.homework;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;


@Named("user")
@SessionScoped
public class User implements Serializable {

    @EJB
    private UserEJB userEJB;

    private String name;
    private Long age;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public void submit() {
        if (age > 17){
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("success.xhtml");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("Access denied for: " + name + " You aren't 18 old!"));
        }
    }
}
