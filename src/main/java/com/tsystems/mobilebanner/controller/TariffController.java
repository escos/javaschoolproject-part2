package com.tsystems.mobilebanner.controller;

import com.tsystems.mobilebanner.jms.Receiver;
import com.tsystems.mobilebanner.model.TariffSimpleDto;
import com.tsystems.mobilebanner.model.TariffSimpleDtos;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named("tariffs")
@Singleton
public class TariffController implements Serializable{

    private final static Logger logger = Logger.getLogger(TariffController.class);

    @EJB
    private TariffSimpleDtos tariffs;

    public TariffSimpleDtos getTariffs() {
        logger.info("Date transmit to jsf");
        return tariffs;
    }

    public void setTariffs(List<TariffSimpleDto> tariffSimpleDtos, Long idTariff) {
        this.tariffs.setTariffSimpleDtos(tariffSimpleDtos);
        logger.info("Set tariffSimpleDtos");
        this.tariffs.setIdTariff(idTariff);
        logger.info("Set idTariff");
    }
}
