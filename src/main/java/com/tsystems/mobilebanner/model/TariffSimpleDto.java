package com.tsystems.mobilebanner.model;

import lombok.Getter;
import lombok.Setter;

import javax.ejb.Stateless;
import java.io.Serializable;
import java.util.Set;

@Stateless
public class TariffSimpleDto implements Serializable {

    @Setter @Getter private Long id;

    @Setter @Getter private String tittle;

    @Setter @Getter private Integer price;

    @Setter @Getter private Set<OptionSimpleDto> options;
}
