package com.tsystems.mobilebanner.model;

import lombok.Getter;
import lombok.Setter;

import javax.ejb.Stateless;
import java.io.Serializable;
import java.util.List;

@Stateless
public class TariffSimpleDtos implements Serializable {

    @Getter @Setter private List<TariffSimpleDto> tariffSimpleDtos;

    @Getter @Setter private Long idTariff;
}
