package com.tsystems.mobilebanner.model;

import lombok.Getter;
import lombok.Setter;

import javax.ejb.Stateless;
import java.io.Serializable;

@Stateless
public class OptionSimpleDto implements Serializable {

    @Setter @Getter private Long id;
    @Setter @Getter private String tittle;
    @Setter @Getter private Integer cost;
    @Setter @Getter private Integer price;
}
